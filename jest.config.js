/** @type {import('ts-jest/dist/types').InitialOptionsTsJest} */
module.exports = {
	preset: "ts-jest",
	testEnvironment: "jsdom",
	moduleNameMapper: {
		"\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$":
			"<rootDir>/src/__mocks__/fileMock.ts",
		"\\.(css|pcss)$": "<rootDir>/node_modules/jest-css-modules",
		"^@utils(.*)$": "<rootDir>/src/utils$1",
		"^@components(.*)$": "<rootDir>/src/components$1",
		"^@base(.*)$": "<rootDir>/src$1",
	},
	collectCoverage: true,
};
