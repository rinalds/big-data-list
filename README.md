# Simple React List component

## Test

1. yarn install
2. yarn serve to start dev environment

## Build

1. yarn install
2. yarn build

## Features

-   Written in TypeScript
-   100% test coverage
-   Can use generics to specify data type

## For testing

-   Validate typescript - yarn validate-ts
-   Validate eslint - yarn lint
-   Run tests - yarn test
