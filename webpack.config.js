/* eslint-disable @typescript-eslint/no-var-requires */
const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

const isProduction = process.env.NODE_ENV == "production";

const config = {
	entry: "./src/index.tsx",
	output: {
		path: path.resolve(__dirname, "dist"),
	},
	devServer: {
		open: true,
		host: "localhost",
	},
	plugins: [
		new HtmlWebpackPlugin({
			template: "index.html",
		}),
	],
	module: {
		rules: [
			{
				test: /\.(ts|tsx)$/i,
				loader: "ts-loader",
				exclude: ["/node_modules/"],
			},
			{
				test: /\.pcss$/i,
				use: [
					"style-loader",
					{
						loader: "css-loader",
						options: {
							modules: {
								localIdentName: "[local]--[hash:base64:5]",
							},
						},
					},
					"postcss-loader",
				],
			},
			{
				test: /\.(eot|svg|ttf|woff|woff2|png|jpg|gif)$/i,
				type: "asset",
			},
		],
	},
	resolve: {
		extensions: [".tsx", ".ts", ".js"],
		alias: {
			"@components": path.resolve(__dirname, "src/components/"),
			"@base": path.resolve(__dirname, "src/"),
			"@utils": path.resolve(__dirname, "src/utils/"),
		},
	},
};

module.exports = () => {
	if (isProduction) {
		config.mode = "production";
		config.plugins.push(
			new MiniCssExtractPlugin({
				filename: "[name].[contenthash].css",
			})
		);
		config.module.rules[1].use[0] = MiniCssExtractPlugin.loader;
	} else {
		config.mode = "development";
	}
	return config;
};
