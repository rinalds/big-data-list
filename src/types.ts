export type TId = string | number;

export interface TBaseItem {
	id: TId;
}

export interface Item extends TBaseItem {
	id: number;
	name: string;
	description: string;
	link: string;
}
