import React, { useCallback } from "react";

import { ListItem } from "@components/ListItem/ListItem";

import styles from "./List.pcss";
import { TBaseItem, TId } from "@base/types";

export interface ListProps<T extends TBaseItem> {
	data: T[];
	checkedIds: TId[];
	renderInfo: (item: T) => JSX.Element;
	onUpdateChecked: (checkedItems: TId[]) => void;
}

export const PureList = <T extends TBaseItem>({
	data,
	checkedIds,
	renderInfo,
	onUpdateChecked,
}: ListProps<T>) => {
	console.log("checkedIds", checkedIds);
	const onChange = useCallback(
		(checked: boolean, itemId: TId) => {
			if (checked) {
				console.log("checkedIds", checkedIds, itemId);
				onUpdateChecked([...checkedIds, itemId]);
			} else {
				console.log("DO THIS");
				onUpdateChecked(checkedIds.filter((id) => id !== itemId));
			}
		},
		[onUpdateChecked, checkedIds]
	);

	return (
		<div className={styles.list}>
			{data.map((item) => (
				<ListItem
					key={item.id}
					renderInfo={renderInfo}
					item={item}
					checked={checkedIds.indexOf(item.id) > -1}
					onChange={onChange}
				/>
			))}
		</div>
	);
};

export const List = React.memo(PureList) as typeof PureList;
