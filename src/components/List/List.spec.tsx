import React from "react";
import { render, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom";

import { Item } from "@base/types";

import { List } from "./List";

const testData: Item[] = [
	{
		id: 1,
		name: "Bahringer - Jenkins",
		description: "3508 Katelin Stream Apt. 300",
		link: "http://pete.name",
	},
	{
		id: 2,
		name: "Murray - McClure",
		description: "118 Lois Fork Apt. 319",
		link: "http://vernie.info",
	},
	{
		id: 3,
		name: "Littel, Halvorson and Hand",
		description: "6016 Gail Stravenue Apt. 223",
		link: "http://nelda.info",
	},
	{
		id: 4,
		name: "Deckow - Jacobson",
		description: "1771 Cleve Ridge Suite 512",
		link: "http://daphnee.org",
	},
	{
		id: 5,
		name: "Kozey LLC",
		description: "9361 White Drive Suite 946",
		link: "https://rico.info",
	},
];

describe("List", () => {
	it("should generate list", () => {
		const { container } = render(
			<List
				data={testData}
				checkedIds={[]}
				renderInfo={(item) => (
					<div>
						{item.name}, {item.description}, {item.link}
					</div>
				)}
				onUpdateChecked={jest.fn()}
			/>
		);

		expect(container.getElementsByClassName("list")).toMatchSnapshot();
	});

	it("should be able to click and check box", () => {
		const data: Item[] = testData;

		const onUpdateCheckedMock = jest.fn();

		const props = {
			data,
			checkedIds: [],
			renderInfo: (item: Item) => (
				<div>
					{item.name}, {item.description}, {item.link}
				</div>
			),
			onUpdateChecked: onUpdateCheckedMock,
		};

		const { container, rerender } = render(<List {...props} />);

		fireEvent.click(
			container.querySelector("input[type='checkbox']") as Element
		);
		expect(onUpdateCheckedMock).toBeCalledWith([1]);
		jest.clearAllMocks();

		rerender(<List {...props} checkedIds={[1, 2]} />);
		fireEvent.click(
			container
				.querySelectorAll("input[type='checkbox']")
				.item(2) as Element
		);
		expect(onUpdateCheckedMock).toBeCalledWith([1, 2, 3]);
		jest.clearAllMocks();

		rerender(<List {...props} checkedIds={[1, 2, 3]} />);
		fireEvent.click(
			container
				.querySelectorAll("input[type='checkbox']")
				.item(2) as Element
		);
		expect(onUpdateCheckedMock).toBeCalledWith([1, 2]);
	});
});
