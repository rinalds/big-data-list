import React, { memo, useCallback } from "react";

import { CheckBox } from "@components/CheckBox/CheckBox";
import { TBaseItem, TId } from "@base/types";

import styles from "./ListItem.pcss";

export interface ListItemProps<T extends TBaseItem> {
	item: T;
	checked: boolean;
	renderInfo: (item: T) => JSX.Element;
	onChange: (checked: boolean, itemId: TId) => void;
}

export function PureListItem<T extends TBaseItem>({
	item,
	checked,
	renderInfo,
	onChange,
}: ListItemProps<T>) {
	const handleChange = useCallback(
		(checked: boolean) => {
			onChange(checked, item.id);
		},
		[item, onChange]
	);
	return (
		<div className={styles.listItem}>
			<CheckBox checked={checked} onChange={handleChange} />
			<div className={styles.listItemInfo}>{renderInfo(item)}</div>
		</div>
	);
}

export const ListItem = memo(PureListItem) as typeof PureListItem;
