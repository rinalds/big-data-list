import React, { useCallback } from "react";

import styles from "./CheckBox.pcss";
import check from "../../assets/check.svg";

export interface CheckBoxProps {
	checked: boolean;
	onChange: (checked: boolean) => void;
}

export function CheckBox({ checked, onChange }: CheckBoxProps) {
	const handleChange = useCallback(
		(e: React.ChangeEvent<HTMLInputElement>) => {
			onChange(e.target.checked);
		},
		[onChange]
	);

	return (
		<div className={styles.checkBox}>
			<input type="checkbox" checked={checked} onChange={handleChange} />
			<img src={check} />
		</div>
	);
}
