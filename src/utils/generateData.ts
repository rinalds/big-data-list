import faker from "faker";

export function generateData(numberOfItems = 100) {
	return Array(numberOfItems)
		.fill("")
		.map((a, index) => ({
			id: index + 1,
			name: faker.company.companyName(),
			description: faker.address.streetAddress(true),
			link: faker.internet.url(),
		}));
}
