import React, { useCallback, useEffect, useState } from "react";
import ReactDOM from "react-dom";

import { List } from "@components/List/List";
import { Item, TId } from "@base/types";
import { generateData } from "@utils/generateData";

import "./main.pcss";

const App = () => {
	const [data, setData] = useState<Item[]>([]);
	const [checkedIds, setCheckedIds] = useState<TId[]>([]);

	useEffect(() => {
		setData(generateData(100));
	}, []);

	const renderInfo = useCallback(
		(item: Item) => (
			<>
				<h1>{item.name}</h1>
				<p>{item.description}</p>
				<p>{item.link}</p>
			</>
		),
		[]
	);

	return (
		<div>
			<h1>Selected items: {checkedIds.join(", ")}</h1>
			<List<Item>
				data={data}
				checkedIds={checkedIds}
				renderInfo={renderInfo}
				onUpdateChecked={setCheckedIds}
			/>
		</div>
	);
};

const el = document.querySelector("#app") as HTMLDivElement;

ReactDOM.render(<App />, el);
